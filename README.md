# Albion Event Fetcher

This tool polls the Albion recent events API and persists them to a SQL database.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)
