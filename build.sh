#!/bin/bash

build_fetcher() {
    go build -o fetcher cmd/fetcher/main.go
}

build_persister() {
    go build -o persister cmd/persister/main.go
}

if [ "$1" == "fetcher" ]; then
    build_fetcher
elif [ "$1" == "persister" ]; then
    build_persister
else
    build_fetcher
    build_persister
fi
