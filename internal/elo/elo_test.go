package elo

import "testing"

func TestWinnerProb_HigherRatingGivesHigherWinProb(t *testing.T) {
	// Arrange
	highRating := 1200
	lowRating := 1000

	// Act
	highWinnerProb := winnerProb(highRating, lowRating, 1000, 1000)
	lowWinnerProb := winnerProb(lowRating, highRating, 1000, 1000)

	// Assert
	if highWinnerProb < lowWinnerProb {
		t.Fatalf("Expected highWinnerProb to be greater than lowWinnerProb: %f > %f\n", highWinnerProb, lowWinnerProb)
	}
}

func TestWinnerProb_HigheRatingGivesWinProbOver50(t *testing.T) {
	// Arrange
	winnerRating := 1200
	loserRating := 1000

	// Act
	prob := winnerProb(winnerRating, loserRating, 1000, 1000)

	if prob <= 0.5 {
		t.Fatalf("Expected winner prob to be > 0.5: %f > 0.5\n", prob)
	}
}

func TestWinnerProb_LowerRatingGivesWinProbUnder50(t *testing.T) {
	// Arrange
	winnerRating := 1000
	loserRating := 1200

	// Act
	prob := winnerProb(winnerRating, loserRating, 1000, 1000)

	if prob >= 0.5 {
		t.Fatalf("Expected winner prob to be < 0.5: %f > 0.5\n", prob)
	}
}

func TestWinnerProb_HigherIpGivesHigherWinProb(t *testing.T) {
	// Arrange
	highIp := 1200
	lowIp := 1000

	// Act
	highWinnerProb := winnerProb(1000, 1000, highIp, lowIp)
	lowWinnerProb := winnerProb(1000, 1000, lowIp, highIp)

	// Assert
	if highWinnerProb < lowWinnerProb {
		t.Fatalf("Expected highWinnerProb to be greater than lowWinnerProb: %f > %f\n", highWinnerProb, lowWinnerProb)
	}
}

func TestWinnerProb_HigheIpGivesWinProbOver50(t *testing.T) {
	// Arrange
	winnerIp := 1200
	loserIp := 1000

	// Act
	prob := winnerProb(1000, 1000, winnerIp, loserIp)

	if prob <= 0.5 {
		t.Fatalf("Expected winner prob to be > 0.5: %f > 0.5\n", prob)
	}
}

func TestWinnerProb_LowerIpGivesWinProbUnder50(t *testing.T) {
	// Arrange
	winnerIp := 1000
	loserIp := 1200

	// Act
	prob := winnerProb(1000, 1000, winnerIp, loserIp)

	if prob >= 0.5 {
		t.Fatalf("Expected winner prob to be < 0.5: %f > 0.5\n", prob)
	}
}

func TestCalc_SameKGivesEqualPointTransfer(t *testing.T) {
	// Arrange
	winnerRating := 1500
	loserRating := 1200

	// Act
	winnerPts, loserPts := CalcPoints(winnerRating, loserRating, 1000, 1000)

	if winnerPts != loserPts {
		t.Fatalf("Expected equal point transfer %d == %d\n", winnerPts, loserPts)
	}
}

func TestCalc_DifferentKGivesUnequalPointTransfer(t *testing.T) {
	// Arrange
	winnerRating := 2400
	loserRating := 2200

	// Act
	winnerPts, loserPts := CalcPoints(winnerRating, loserRating, 1000, 1000)

	if winnerPts >= loserPts {
		t.Fatalf("Expected lower rank loser to lose more than higher rank winner %d > %d\n", loserPts, winnerPts)
	}
}
