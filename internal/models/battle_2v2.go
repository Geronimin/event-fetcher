package models

import (
	"time"
)

type Battle2v2 struct {
	BattleId       int
	Timestamp      time.Time
	Winner1        string
	Winner2        string
	Loser1         string
	Loser2         string
	Winner1Loadout []byte
	Winner2Loadout []byte
	Loser1Loadout  []byte
	Loser2Loadout  []byte
	events         []*BattleEvent
}

type BattleEvent struct {
	BattleId      int       `db:"battle_id"`
	EventId       int       `db:"event_id"`
	Timestamp     time.Time `db:"time"`
	KillerName    string    `db:"killer_name"`
	VictimName    string    `db:"victim_name"`
	KillerLoadout []byte    `db:"killer_loadout"`
	VictimLoadout []byte    `db:"victim_loadout"`
	KillerIp      int       `db:"killer_ip"`
	VictimIp      int       `db:"victim_ip"`
}
