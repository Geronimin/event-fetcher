package persister

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	"albion-event-fetcher/internal/battle"
	"albion-event-fetcher/internal/rnkr"
	"github.com/getsentry/sentry-go"
	"github.com/jmoiron/sqlx"
)

type battleExtractor2v2 struct {
	db    *sqlx.DB
	rater *rnkr.Rater
}

func BattleExtractor2v2(db *sqlx.DB) *battleExtractor2v2 {
	extractor := &battleExtractor2v2{
		db:    db,
		rater: rnkr.DefaultRater(),
	}

	return extractor
}

func (h *battleExtractor2v2) Start() {
	ticker := time.NewTicker(15 * time.Minute)
	err := h.extract()
	if err != nil {
		log.Println(err)
	}
	go withRestart(func() {
		for range ticker.C {
			log.Println("Extracting 2v2 Events")
			err := h.extract()
			if err != nil {
				log.Println(err)
				sentry.CaptureException(fmt.Errorf("2v2 Battle Extraction Failed: %w\n", err))
			}
		}
	})
}

func (h *battleExtractor2v2) extract() error {
	row := h.db.QueryRow("select max(battle_id) from 2v2_hg_battle")
	var last_battle_id sql.NullInt64
	err := row.Scan(&last_battle_id)
	if err == sql.ErrNoRows {
		// noop
	} else if err != nil {
		return fmt.Errorf("Failed to fetch last battle_id: %w", err)
	}

	log.Println("Fetched last battle id", last_battle_id.Int64)

	query := `
    with
    2_3_event_battles as (
        select battle_id
        from events as e
        join killers as k on k.event_id = e.event_id
        join victims as v on v.event_id = e.event_id
        where
        e.battle_id is not null
        and e.time > DATE_SUB(NOW(), INTERVAL 3 DAY)
        and e.battle_id > ?
        group by battle_id
        having count(distinct e.event_id) >=2 and count(distinct e.event_id) <= 3
    ),
    battles as (
        select e.battle_id, group_concat(distinct e.event_id) from events as e
        join 2_3_event_battles as b on e.battle_id = b.battle_id
        join killers as k on k.event_id = e.event_id
        join victims as v on v.event_id = e.event_id
        where e.participant_count <= 4
        and e.party_size <= 2
        and ((k.item_power >= 1000 and k.item_power < 1205) or (k.is_primary = 0 and k.item_power = 0))
        and ((v.item_power >= 1000 and v.item_power < 1205) or v.item_power = 0)
        and (k.kill_fame > 0 OR k.damage_done > 0 OR k.healing_done > 0)
        and e.time > DATE_SUB(NOW(), INTERVAL 3 DAY)
        group by e.battle_id
        having count(distinct e.event_id) >=2 and count(distinct e.event_id) <=3
    )
    select
        b.battle_id,
        e.event_id,
        UNIX_TIMESTAMP(e.time) as time,
        k.name as killer_name,
        v.name as victim_name,
        k.loadout as killer_loadout,
        v.loadout as victim_loadout,
        k.item_power as killer_ip,
        v.item_power as victim_ip
    from
    events as e
    join battles as b on b.battle_id = e.battle_id
    join killers as k on k.event_id = e.event_id
    join victims as v on v.event_id = e.event_id
    order by battle_id asc;`

	rows, err := h.db.Queryx(query, last_battle_id.Int64)

	if err != nil {
		return fmt.Errorf("Failed to fetch events for 2v2 battle extraction: %w", err)
	}
	defer rows.Close()

	lastBattle := int(last_battle_id.Int64)
	var currentBattle *battle.Battle2v2
	for rows.Next() {
		battleEvent := &battle.BattleEvent{}
		err = rows.StructScan(battleEvent)
		if err != nil {
			return fmt.Errorf("Failed to scan battle event for 2v2 battle extractor: %w", err)
		}

		if battleEvent.BattleId != lastBattle {
			if currentBattle != nil {
				if currentBattle.Extract() {
					err = h.saveBattle(currentBattle)
					if err != nil {
						return err
					}
				}
			}
			lastBattle = battleEvent.BattleId
			currentBattle = &battle.Battle2v2{}
		}
		currentBattle.AddEvent(battleEvent)
	}

	return nil
}

func (h *battleExtractor2v2) saveBattle(b *battle.Battle2v2) error {
	_, err := h.db.NamedExec(`
    INSERT INTO 2v2_hg_battle (
        battle_id, time, winner_1, winner_2, loser_1, loser_2, winner_1_loadout, winner_2_loadout, loser_1_loadout, loser_2_loadout
    ) VALUES (
        :battle_id, FROM_UNIXTIME(:time), :winner_1, :winner_2, :loser_1, :loser_2, :winner_1_loadout, :winner_2_loadout, :loser_1_loadout, :loser_2_loadout
    )`, b)

	if err != nil {
		return err
	}

	err = h.updateFav2v2Weapons(b)
	if err != nil {
		return err
	}

	err = h.updateRatings(b)

	return err
}

func (h *battleExtractor2v2) updateRatings(b *battle.Battle2v2) error {
	ratings, err := h.getPlayerRatings([]string{b.Winner1, b.Winner2, b.Loser1, b.Loser2})
	if err != nil {
		return err
	}

	teams := [][]*rnkr.Rating{
		{ratings[b.Winner1], ratings[b.Winner2]},
		{ratings[b.Loser1], ratings[b.Loser2]},
	}
	newTeams, err := h.rater.UpdateRatings(teams, []uint{1, 2})
	if err != nil {
		return err
	}

	newRatings := teamsToMap(newTeams)
	err = h.upsertRatings(newRatings)
	if err != nil {
		return err
	}

	err = h.writeRatingTransactions(b.BattleId, ratings, newRatings)
	if err != nil {
		return err
	}

	return nil
}

func (h *battleExtractor2v2) getPlayerRatings(names []string) (map[string]*rnkr.Rating, error) {
	query, args, _ := sqlx.In("select name, skill, uncertainty from rating_2v2 where name in (?)", names)
	query = h.db.Rebind(query)
	rows, err := h.db.Query(query, args...)

	if err != nil {
		return nil, fmt.Errorf("Failed to players' current 2v2 ratings: %w", err)
	}
	defer rows.Close()

	result := make(map[string]*rnkr.Rating)
	for rows.Next() {
		var skill float64
		var uncertainty float64
		var name string
		rows.Scan(&name, &skill, &uncertainty)

		result[name] = rnkr.NewRating(name, skill, uncertainty)
	}

	for _, name := range names {
		if _, exists := result[name]; !exists {
			result[name] = rnkr.DefaultRating()
			result[name].Label = name
		}
	}

	return result, nil
}

func (h *battleExtractor2v2) upsertRatings(ratings map[string]*rnkr.Rating) error {
	tx, err := h.db.Beginx()
	if err != nil {
		return err
	}

	for _, rating := range ratings {
		_, err = tx.NamedExec(
			`INSERT INTO rating_2v2 (name, skill, uncertainty, rating)
            VALUES (:name, :skill, :uncertainty, :rating)
            ON DUPLICATE KEY UPDATE skill = :skill, uncertainty = :uncertainty, rating = :rating, last_update= now()`,
			map[string]interface{}{
				"name":        rating.Label,
				"skill":       rating.EstimatedSkill,
				"uncertainty": rating.Uncertainty,
				"rating":      rating.Rating(),
			},
		)
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

func (h *battleExtractor2v2) writeRatingTransactions(battleId int, oldRatings, newRatings map[string]*rnkr.Rating) error {
	tx, err := h.db.Beginx()
	if err != nil {
		return err
	}

	for name, oldRating := range oldRatings {
		var newRating *rnkr.Rating
		if r, exists := newRatings[name]; exists {
			newRating = r
		} else {
			tx.Rollback()
			return fmt.Errorf("old and new ratings didn't have the same players\nold ratings: %v\nnew ratings: %v", oldRatings, newRatings)
		}
		_, err = tx.NamedExec(
			`INSERT INTO rating_2v2_transaction (battle_id, name, rating_change)
            VALUES (:battle_id, :name, :rating_change)`,
			map[string]interface{}{
				"battle_id":     battleId,
				"name":          name,
				"rating_change": newRating.Rating() - oldRating.Rating(),
			},
		)
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

func (h *battleExtractor2v2) updateFav2v2Weapons(b *battle.Battle2v2) error {
	var err error

	err = h.updatePlayerFav2v2Weapons(b.Winner1)
	if err != nil {
		return err
	}
	err = h.updatePlayerFav2v2Weapons(b.Winner2)
	if err != nil {
		return err
	}
	err = h.updatePlayerFav2v2Weapons(b.Loser1)
	if err != nil {
		return err
	}
	err = h.updatePlayerFav2v2Weapons(b.Loser2)
	if err != nil {
		return err
	}

	return nil
}

func (h *battleExtractor2v2) updatePlayerFav2v2Weapons(name string) error {
	query := `
replace into player_fav_weapon_2v2 (name, weapon)
with battles as (
	(select battle_id, time, winner_1 as name, l.main_hand_item as weapon
	from 2v2_hg_battle as b
	join loadouts as l on b.winner_1_loadout = l.id
	where winner_1 = :name)
	union all
	(select battle_id, time, winner_2 as name, l.main_hand_item as weapon
	from 2v2_hg_battle as b
	join loadouts as l on b.winner_2_loadout = l.id
	where winner_2 = :name)
	union all
	(select battle_id, time, loser_1 as name, l.main_hand_item as weapon
	from 2v2_hg_battle as b
	join loadouts as l on b.loser_1_loadout = l.id
	where loser_1 = :name)
	union all
	(select battle_id, time, loser_2 as name, l.main_hand_item as weapon
	from 2v2_hg_battle as b
	join loadouts as l on b.loser_2_loadout = l.id
	where loser_2 = :name)
	order by battle_id desc limit 25
)
select name, weapon from battles
group by name, weapon
order by count(*) desc limit 1;
`
	_, err := h.db.NamedExec(query, map[string]interface{}{
		"name": name,
	})

	return err
}

func teamsToMap(teams [][]*rnkr.Rating) map[string]*rnkr.Rating {
	result := make(map[string]*rnkr.Rating)

	for _, team := range teams {
		for _, playerRating := range team {
			result[playerRating.Label] = playerRating
		}
	}

	return result
}
