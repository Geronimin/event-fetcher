package persister

import (
	"log"
	"sync"

	"albion-event-fetcher/internal/models"

	"github.com/getsentry/sentry-go"
	"github.com/jmoiron/sqlx"
)

type favWeaponHandler struct {
	db *sqlx.DB
	sync.Mutex
}

func FavWeaponHandler() *favWeaponHandler {
	handler := &favWeaponHandler{}

	return handler
}

func (h *favWeaponHandler) setup(db *sqlx.DB) {
	h.db = db
}

func (h *favWeaponHandler) handleEvents(events []*models.Event) {
	if len(events) == 0 {
		return
	}

	names := make([]string, 0)
	for _, event := range events {
		names = append(names, event.Killer.Name)
	}

	// names to exclude becuase they were updated recently
	query := `select name from player_fav_weapon where name IN (?) and last_update > DATE_SUB(NOW(), INTERVAL 2 DAY)`
	query, args, err := sqlx.In(query, names)

	if err != nil {
		log.Printf("Failed to parameterize exclude list query for updating favorite weapons: %v\n", err)
		sentry.CaptureException(err)
		return
	}

	h.db.Rebind(query)
	rows, err := h.db.Query(query, args...)

	if err != nil {
		log.Printf("Failed to query exclude list for updating favorite weapons: %v\n", err)
		sentry.CaptureException(err)
		return
	}

	defer rows.Close()

	excludedNames := make([]string, 0)
	for rows.Next() {
		var n string
		rows.Scan(&n)
		excludedNames = append(excludedNames, n)
	}

	updateNames := sliceDifference(names, excludedNames)

	if len(updateNames) == 0 {
		return
	}

	h.updateOverallFavoriteWeapon(updateNames)
	h.update1v1FavoriteWeapon(updateNames)
}

func (h *favWeaponHandler) updateOverallFavoriteWeapon(names []string) {
	query := `
replace into player_fav_weapon (name, weapon)
select
	name,
	substring(max(concat(lpad(c, 20, '0'), main_hand_item)), 21) as fav_weapon
from (select name, main_hand_item, count(*) as c from
	(select k.name, l.main_hand_item
	from killers as k
	join loadouts as l on k.loadout = l.id
	join events as e on e.event_id = k.event_id
	where e.time > DATE_SUB(NOW(), INTERVAL 7 DAY) and k.name IN (?)
	union all
	select v.name, l.main_hand_item
	from victims as v
	join loadouts as l on v.loadout = l.id
	join events as e on e.event_id = v.event_id
	where e.time > DATE_SUB(NOW(), INTERVAL 7 DAY) and v.name IN (?)) as tt
	group by name, main_hand_item
) as t
group by name`
	query, args, err := sqlx.In(query, names, names)

	if err != nil {
		log.Printf("Failed to parameterize update fav weapons query: %v\n", err)
		sentry.CaptureException(err)
		return
	}

	h.db.Rebind(query)
	_, err = h.db.Exec(query, args...)

	if err != nil {
		log.Printf("Failed to execute fav weapons update query: %v\n", err)
		sentry.CaptureException(err)
	}
}

func (h *favWeaponHandler) update1v1FavoriteWeapon(names []string) {
	query := `
replace into player_fav_weapon_1v1 (name, weapon)
select
	name,
	substring(max(concat(lpad(c, 20, '0'), main_hand_item)), 21) as fav_weapon
from (select name, main_hand_item, count(*) as c from
	(select k.name, l.main_hand_item
	from killers as k
	join loadouts as l on k.loadout = l.id
	join events as e on e.event_id = k.event_id
	where e.time > DATE_SUB(NOW(), INTERVAL 7 DAY) and k.name IN (?)
	and e.party_size = 1 and e.participant_count = 1
	union all
	select v.name, l.main_hand_item
	from victims as v
	join loadouts as l on v.loadout = l.id
	join events as e on e.event_id = v.event_id
	where e.time > DATE_SUB(NOW(), INTERVAL 7 DAY) and v.name IN (?)
	and e.party_size = 1 and e.participant_count = 1) as tt
	group by name, main_hand_item
) as t
group by name`
	query, args, err := sqlx.In(query, names, names)

	if err != nil {
		log.Printf("Failed to parameterize update fav weapons query: %v\n", err)
		sentry.CaptureException(err)
		return
	}

	h.db.Rebind(query)
	_, err = h.db.Exec(query, args...)

	if err != nil {
		log.Printf("Failed to execute fav weapons update query: %v\n", err)
		sentry.CaptureException(err)
	}
}

func sliceDifference(a, b []string) []string {
	mb := make(map[string]struct{}, len(b))
	for _, x := range b {
		mb[x] = struct{}{}
	}
	var diff []string
	for _, x := range a {
		if _, found := mb[x]; !found {
			diff = append(diff, x)
		}
	}
	return diff
}
