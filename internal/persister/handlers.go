package persister

import (
	"albion-event-fetcher/internal/models"
	"github.com/jmoiron/sqlx"
)

type handler interface {
	handleEvents(event []*models.Event)
	setup(db *sqlx.DB)
}
