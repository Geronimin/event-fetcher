package rnkr

import (
	"fmt"
	"math"
)

const DEFAULT_RANDOMNESS = 25.0 / 6.0

type Rater struct {
	// Fixed value that indicates how much randomness is involved in the game.
	// If the game has no random elements, then only our uncertainty in each
	// players rating will affect our prediction of the outcome.
	randomnessSquared float64
}

type instanceData struct {
	teamSkill    []float64
	teamVariance []float64
	teamOmega    []float64
	teamDelta    []float64
}

func NewRater(randomness float64) *Rater {
	return &Rater{
		randomnessSquared: randomness * randomness,
	}
}

func DefaultRater() *Rater {
	return NewRater(DEFAULT_RANDOMNESS)
}

// This is bayesian rating system was ported from:
//   https://docs.rs/bbt/0.2.0/bbt/
// and is based on this paper:
//   https://jmlr.csail.mit.edu/papers/volume12/weng11a/weng11a.pdf
func (r *Rater) UpdateRatings(teams [][]*Rating, ranks []uint) ([][]*Rating, error) {
	data := &instanceData{}
	var err error

	data.teamSkill, data.teamVariance, err = collectTeamSkill(teams)
	if err != nil {
		return nil, err
	}

	data.teamOmega, data.teamDelta = computeTeamOmegaDelta(
		teams,
		ranks,
		data.teamSkill,
		data.teamVariance,
		r.randomnessSquared,
	)

	result := make([][]*Rating, len(teams))
	for teamIndex, team := range teams {
		result[teamIndex] = computeTeamResult(teamIndex, team, data)
	}

	return result, nil
}

func collectTeamSkill(teams [][]*Rating) ([]float64, []float64, error) {
	teamSkill := make([]float64, len(teams))
	teamVariance := make([]float64, len(teams))

	for teamIndex, teamRatings := range teams {
		if len(teamRatings) == 0 {
			return nil, nil, fmt.Errorf("At least one of the teams contains no players")
		}

		for _, playerRating := range teamRatings {
			teamSkill[teamIndex] += playerRating.EstimatedSkill
			teamVariance[teamIndex] += playerRating.UncertaintySquared
		}
	}

	return teamSkill, teamVariance, nil
}

func computeTeamOmegaDelta(
	teams [][]*Rating,
	ranks []uint,
	teamSkill,
	teamVariance []float64,
	randomness float64,
) ([]float64, []float64) {
	teamOmega := make([]float64, len(teams))
	teamDelta := make([]float64, len(teams))

	for team1Index := range teams {
		for team2Index := range teams {
			if team1Index == team2Index {
				continue
			}

			c := math.Sqrt(teamVariance[team1Index] + teamVariance[team2Index] + 2.0*randomness)
			e1 := math.Exp(teamSkill[team1Index] / c)
			e2 := math.Exp(teamSkill[team2Index] / c)
			piq := e1 / (e1 + e2)
			pqi := e2 / (e1 + e2)
			ri := ranks[team1Index]
			rq := ranks[team2Index]
			var s float64
			if rq > ri {
				s = 1.0
			} else if rq == ri {
				s = 0.5
			} else {
				s = 0.0
			}

			delta := (teamVariance[team1Index] / c) * (s - piq)
			gamma := math.Sqrt(teamVariance[team1Index]) / c
			eta := gamma * (teamVariance[team1Index] / (c * c)) * piq * pqi

			teamOmega[team1Index] += delta
			teamDelta[team1Index] += eta
		}
	}

	return teamOmega, teamDelta
}

func computeTeamResult(teamIndex int, team []*Rating, data *instanceData) []*Rating {
	teamResult := make([]*Rating, len(team))

	for playerIndex, player := range team {
		newSkill := player.EstimatedSkill + (player.UncertaintySquared/data.teamVariance[teamIndex])*data.teamOmega[teamIndex]

		uncertaintyAdj := 1.0 - (player.UncertaintySquared/data.teamVariance[teamIndex])*data.teamDelta[teamIndex]

		if uncertaintyAdj < 0.0001 {
			uncertaintyAdj = 0.0001
		}

		newUncertaintySquared := player.UncertaintySquared * uncertaintyAdj

		teamResult[playerIndex] = &Rating{
			Label:              player.Label,
			EstimatedSkill:     newSkill,
			Uncertainty:        math.Sqrt(newUncertaintySquared),
			UncertaintySquared: newUncertaintySquared,
		}
	}

	return teamResult
}
