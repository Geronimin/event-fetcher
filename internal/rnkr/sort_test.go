package rnkr

import (
	"sort"
	"strconv"
	"testing"
)

func TestSortByRating_ShouldBeSortedByRating(t *testing.T) {
	// Arrange
	ratings := []*Rating{
		NewRating("1", 10.0, 8.0), // -14
		NewRating("4", 24.0, 2.0), // 18
		NewRating("5", 50.0, 5.0), // 35
		NewRating("3", 26.0, 3.0), // 17
		NewRating("2", 30.0, 6.0), // 12
	}

	// Act
	sort.Sort(ByRating(ratings))

	// Assert
	for index, rating := range ratings {
		ratingLabel, _ := strconv.Atoi(rating.Label)
		if ratingLabel != index+1 {
			t.Fatalf("Expected rank %s rating to be at index %d", rating.Label, index)
		}
	}
}
